/**
 * 
 */
package com.example.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author victo
 *
 */
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Image {
	private int id;
	private String title;
	private String url;
}
