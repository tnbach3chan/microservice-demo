/**
 * 
 */
package com.example.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.entity.Gallery;

/**
 * @author victo
 *
 */
@RestController
@RequestMapping("/")
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);  
	
	@Autowired
	private Environment env;
	
	@Autowired
	private RestTemplate restTemplate;

	@RequestMapping("/")
	public String home() {
		return "Gallery Service running at port: " + env.getProperty("local.server.port");
	}

	@RequestMapping("/admin") 
	public String homeAdmin() {
		return "This is the admin area of Gallery service running at port: " + env.getProperty("local.server.port");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/{id}")
	public Gallery getGallery(@PathVariable final int id) {
		LOGGER.info("Creating gallery object ...");
		// create gallery object
		Gallery gallery = new Gallery();
		gallery.setId(id);

		// get list of available images
		List<Object> images = restTemplate.getForObject("http://image-service/images/", List.class);
		gallery.setImages(images);
		LOGGER.info("Returning images ...");

		return gallery;
	}
}
